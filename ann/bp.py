# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2

back propagation example
"""

import numpy as np


def f(x):
    """
    logistic function 
    """
    return 1. / (1 + np.exp(-x))



def main():
    f_vec = np.vectorize(f)

    # input data
    X = np.random.rand(2) * 20
    Y = np.random.rand()
    W1 = np.random.rand(2, 3)
    W2 = np.random.rand(3, 2)
    W3 = np.random.rand(2, 1)

    step = 0.5

    for idx in range(30000):
        z1 = np.dot(W1.T, X)
        o1 = f_vec(z1)

        z2 = np.dot(W2.T, o1)
        o2 = f_vec(z2)

        z3 = np.dot(W3.T, o2)
        o3 = f_vec(z3)
        diff = o3-Y
        cost = diff*diff/2
        print("Y: {}, hat_Y:{}, diff:{} cost:{}".format(Y, o3, diff, cost))


        # back propagation
        # layer3
        delta_3 = diff * (1 - o3[0]) * o3[0]

        d_3_11 = delta_3 * o2[0]
        d_3_21 = delta_3 * o2[1]

        # layer2
        delta_2_1 = delta_3 * W3[0][0] * (1-o2[0]) * o2[0]
        d_2_11 = delta_2_1 * o1[0]
        d_2_21 = delta_2_1 * o1[1]
        d_2_31 = delta_2_1 * o1[2]

        delta_2_2 = delta_3 * W3[1][0] * (1 - o2[0]) * o2[0]
        d_2_12 = delta_2_2 * o1[0]
        d_2_22 = delta_2_2 * o1[1]
        d_2_32 = delta_2_2 * o1[2]

        # layer 1
        delta_1_1_s1  = delta_2_1 * W2[0][0] * (1-o1[0]) * o1[0]
        delta_1_1_s2 = delta_2_2 * W2[0][1] * (1-o1[0]) * o1[0]
        d_1_11 = (delta_1_1_s1 + delta_1_1_s2) * X[0]
        d_1_21 = (delta_1_1_s1 + delta_1_1_s2) * X[1]

        delta_1_2_s1 =  delta_2_1 * W2[0][0] * (1-o1[1]) * o1[1]
        delta_1_2_s2 = delta_2_2 * W2[0][1] * (1 - o1[1]) * o1[1]
        d_1_12 = (delta_1_2_s1 + delta_1_2_s2 ) * X[0]
        d_1_22 = (delta_1_2_s1 + delta_1_2_s2 ) * X[1]

        delta_1_3_s1 = delta_2_1 * W2[0][0] * (1 - o1[2]) * o1[2]
        delta_1_3_s2 = delta_2_2 * W2[0][1] * (1 - o1[2]) * o1[2]
        d_1_13 = (delta_1_3_s1 + delta_1_3_s2) * X[0]
        d_1_23 = (delta_1_3_s1 + delta_1_3_s2) * X[1]

        W1[0][0] -= step * d_1_11
        W1[0][1] -= step * d_1_12
        W1[0][2] -= step * d_1_13
        W1[1][0] -= step * d_1_21
        W1[1][1] -= step * d_1_22
        W1[1][2] -= step * d_1_23

        W2[0][0] -= step * d_2_11
        W2[0][1] -= step * d_2_12
        W2[1][0] -= step * d_2_21
        W2[1][1] -= step * d_2_22
        W2[2][0] -= step * d_2_31
        W2[2][1] -= step * d_2_32

        W3[0][0] -= step * d_3_11
        W3[1][0] -= step * d_3_21


if __name__ == '__main__':
    main()
