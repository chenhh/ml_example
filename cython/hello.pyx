# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""



cpdef int fib(int n):
    if n == 0 or n == 1:
        return 1
    else:
        return fib(n - 2) + fib(n - 1)


