# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

from distutils.core import setup
from Cython.Build import cythonize

setup(
    ext_modules=cythonize("hello.pyx")
)
