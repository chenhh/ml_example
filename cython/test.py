# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

from hello import fib
from time import time

def test_fib():
    n = 100
    ts = time()
    for _ in range(n):
        fib(25)
    avg_t = (time() - ts) / n

    print("python fib : {:.8f}s".format(avg_t))

if __name__ == "__main__":
    test_fib()