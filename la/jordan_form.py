# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

import numpy as np

from sympy import Matrix

# a = np.array([
#     [1,4,0,0,3],
#     [0,1,0,0,0],
#     [0,0,2,0,-1],
#     [0,0,0,2,0],
#     [0,0,0,0,2]
# ])

# a = np.array([
#     [2,5,0,0,1],
#     [0,2,0,0,0],
#     [0,0,-1,0,0],
#     [0,0,0,-1,0],
#     [0,0,0,0,-1]
# ])

a = np.array([
    [2,-1,0,1],
    [0,3,-1,0],
    [0,1,1,0],
    [0,-1,0,3]
])


m = Matrix(a)

print(m)


# Jordan form傳回的是上移矩陣
P, J = m.jordan_form()

print(J)
print (P)
