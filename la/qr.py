# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
QR-decomposition
"""

import numpy as np
import scipy.linalg as la


# M,N = 4, 3
# A =np.random.rand(M,N)

A = np.array(
    [
        [1,-1],
        [1,1],
        [1,2]
    ]

)

print (la.qr(A))
