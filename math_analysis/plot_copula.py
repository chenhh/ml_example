# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v3
"""

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


def non_decreasing_function():
    xs = np.linspace(-1, 1, 100)
    ys = np.linspace(0, 10, 100)

    xs, ys = np.meshgrid(xs, ys)
    zs = (xs + 1) * (np.exp(ys) - 1) / (xs + 2 * np.exp(ys) - 1)

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot_surface(xs, ys, zs, linewidth=0, antialiased=True)

    plt.show()


def plot_product_copula():
    """
    product copula: C(u,v)=u*v
    """

    xs = np.linspace(0, 1, 100)
    ys = np.linspace(0, 1, 100)
    xs, ys = np.meshgrid(xs, ys)
    zs = xs * ys

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot_surface(xs, ys, zs, linewidth=0, antialiased=True)
    plt.title("product copula")

    fig = plt.figure(2)
    ax = fig.gca()
    ax.contourf(xs, ys, zs, 10, alpha=.75, cmap=plt.cm.hot)
    plt.title("product copula")

    plt.show()


def Frechet_Hoeffding_upper_bound_copula():
    """
    copula: C(u,v)=min(u,v)
    """

    xs = np.linspace(0, 1, 100)
    ys = np.linspace(0, 1, 100)
    xs, ys = np.meshgrid(xs, ys)
    zs = np.minimum(xs, ys)

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot_surface(xs, ys, zs, linewidth=0, antialiased=True)
    plt.title("Frechet Hoeffding upper bound copula")

    fig = plt.figure(2)
    ax = fig.gca()
    ax.contourf(xs, ys, zs, 10, alpha=.75, cmap=plt.cm.hot)
    plt.title("Frechet Hoeffding upper bound copula")

    plt.show()


def Frechet_Hoeffding_lower_bound_copula():
    """
    copula: C(u,v)=max(u+v-1,0)
    """

    xs = np.linspace(0, 1, 100)
    ys = np.linspace(0, 1, 100)
    xs, ys = np.meshgrid(xs, ys)
    zs = np.maximum(xs + ys - 1, 0)

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.view_init(elev=37., azim=-130)
    ax.plot_surface(xs, ys, zs, linewidth=0, antialiased=True)
    plt.title("Frechet Hoeffding lower bound copula")

    fig = plt.figure(2)
    ax = fig.gca()
    ax.contourf(xs, ys, zs, 10, alpha=.75, cmap=plt.cm.hot)
    plt.title("Frechet Hoeffding lower bound copula")

    plt.show()


def Mardia_copula(theta=-1):
    """
    one parameter
    c*c*(1+c)/2*M(u,v)+(1-c*c)*P(u,v)+c*(1-c)/2*W(u,v)
    """
    xs = np.linspace(0, 1, 100)
    ys = np.linspace(0, 1, 100)
    xs, ys = np.meshgrid(xs, ys)
    zs = ((theta * theta * (theta + 1)) / 2 * np.minimum(xs, ys) +
          (1 - theta * theta) * xs * ys +
          (theta * theta * (1 - theta)) * np.maximum(xs + ys - 1, 0)
          )

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.view_init(elev=37., azim=-130)
    ax.plot_surface(xs, ys, zs, linewidth=0, antialiased=True)
    plt.title("Mardia copula $\\theta$={}".format(theta))

    fig = plt.figure(2)
    ax = fig.gca()
    # cm=colormap https://matplotlib.org/users/colormaps.html
    ax.contourf(xs, ys, zs, 10, alpha=.75, cmap=plt.cm.hot)
    plt.title("Mardia copula $\\theta$={}".format(theta))
    plt.show()


def Cuadras_Auge_copula(theta=0):
    """
    0<=theta <=1
    """
    if not 0 <= theta <= 1:
        raise ValueError("{} > 1 or {} < 0.".format(theta, theta))
    us = np.linspace(0, 1, 100)
    vs = np.linspace(0, 1, 100)
    us, vs = np.meshgrid(us, vs)
    zs = (np.minimum(us, vs)) ** (theta) * (us * vs) ** (1 - theta)

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.view_init(elev=37., azim=-130)
    ax.plot_surface(us, vs, zs, linewidth=0, antialiased=True)
    plt.title("Cuadras Auge copula $\\theta$={}".format(theta))

    fig = plt.figure(2)
    ax = fig.gca()
    # cm=colormap https://matplotlib.org/users/colormaps.html
    ax.contourf(us, vs, zs, 10, alpha=.75, cmap=plt.cm.hot)
    plt.title("Cuadras Auge copula $\\theta$={}".format(theta))

    plt.show()


def copula_sampling(n_sample=1000):
    """
    H(x,y)= (x+1)*(e^y -1)/(x+2*e^y-1), if (x,y)\in[-1,1] \times [0, \infty)
          = 1-e^(-y)                  , if (x,y) \in [1,\infty] \times [0,
          \infty],
          = 0, otherwise
    C(u,v) = u*v/(u+v-u*v),
    c_u(v) = (v/(u+v-u*v))**2
    c_v(v)_inv = u*\sqrt(t)/(1-(1-u)*\sqrt(t))

    3d histogram plot
    """
    u = np.random.rand(n_sample)
    t = np.random.rand(n_sample)
    sqrt_t = np.sqrt(t)

    v = u * sqrt_t / (1 - (1 - u) * sqrt_t)
    xs = 2 * u - 1
    ys = -np.log(1 - v)

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.view_init(elev=37., azim=-130)
    hist, xedges, yedges = np.histogram2d(xs, ys, bins=(10, 10))
    xpos, ypos = np.meshgrid(xedges[:-1] + xedges[1:],
                             yedges[:-1] + yedges[1:])

    xpos = xpos.flatten() / 2.
    ypos = ypos.flatten() / 2.
    zpos = np.zeros_like(xpos)

    dx = xedges[1] - xedges[0]
    dy = yedges[1] - yedges[0]
    dz = hist.flatten()

    ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color='b', zsort='average')
    plt.xlabel("X")
    plt.ylabel("Y")

    plt.show()


def example_2_5():
    xs = np.linspace(-1, 4, 100)
    ys = np.linspace(0, 4, 100)
    xs, ys = np.meshgrid(xs, ys)
    e_y = np.exp(ys)
    e_ny = np.exp(-ys)
    zs = ((xs <= 1) * (xs + 1) * (e_y - 1) / (xs + 2 * e_y - 1) +
          (xs > 1) * (1 - e_ny))

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.view_init(elev=37., azim=-130)
    ax.plot_surface(xs, ys, zs, linewidth=0, antialiased=True)
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.title("H(x,y)")

    fig = plt.figure(2)
    ax = fig.gca()
    # cm=colormap https://matplotlib.org/users/colormaps.html
    ax.contourf(xs, ys, zs, 10, alpha=.75, cmap=plt.cm.hot)
    plt.title("H(x,y)")
    plt.xlabel("X")
    plt.ylabel("Y")

    fig = plt.figure(3)
    us = np.linspace(0, 1, 100)
    vs = np.linspace(0, 1, 100)
    us, vs = np.meshgrid(us, vs)
    zs = (us * vs) / (us + vs - us * vs)

    ax = fig.gca(projection='3d')
    ax.view_init(elev=37., azim=-130)
    ax.plot_surface(us, vs, zs, linewidth=0, antialiased=True)
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.title("Copula C")

    fig = plt.figure(4)
    ax = fig.gca()
    ax.contourf(us, vs, zs, 10, alpha=.75, cmap=plt.cm.hot)
    plt.title("Copula")
    plt.xlabel("U")
    plt.ylabel("V")

    plt.show()


def Gumbel_bivariate_exp_dist(theta=0):
    xs = np.linspace(0, 10, 100)
    ys = np.linspace(0, 10, 100)

    xs, ys = np.meshgrid(xs, ys)
    zs = 1 - np.exp(-xs) - np.exp(-ys) + np.exp(-xs - ys - theta * xs * ys)

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.view_init(elev=37., azim=-130)
    ax.plot_surface(xs, ys, zs, linewidth=0, antialiased=True)
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.title("Gumbel bivariate exp distribution: $\\theta$={}".format(theta))

    fig = plt.figure(2)
    ax = fig.gca()
    ax.contourf(xs, ys, zs, 10, alpha=.75, cmap=plt.cm.hot)
    plt.title("Gumbel bivariate exp distribution: $\\theta$={}".format(theta))
    plt.xlabel("X")
    plt.ylabel("Y")

    plt.show()


if __name__ == '__main__':
    # non_decreasing_function()
    # plot_product_copula()
    # Frechet_Hoeffding_upper_bound_copula()
    # Frechet_Hoeffding_lower_bound_copula()
    # Mardia_copula(1)
    # Cuadras_Auge_copula(1)
    # copula_sampling(100000)
    # example_2_5()
    Gumbel_bivariate_exp_dist(theta=1)
