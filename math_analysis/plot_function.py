# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

import matplotlib.pyplot as plt
import numpy as np


def polynomial_function():
    xs = np.linspace(-3., 2., 100)
    ys = [1 + x + x * x for x in xs]
    plt.title("polynomial function: $1+x+x^2$")
    plt.plot(xs, ys)
    plt.show()


def even_power_function():
    xs = np.linspace(-1., 1., 100)

    for idx in range(1, 5):
        power = 2*idx
        plt.subplot(int("22{}".format(idx)))
        y = [x**power for x in xs]
        plt.title('$f(x)=x^{}$'.format(power))
        plt.plot(xs, y)

    plt.show()


def odd_power_function():
    xs = np.linspace(-1., 1., 100)
    for idx in range(1, 5):
        power = 2 * idx-1
        plt.subplot(int("22{}".format(idx)))
        y = [x ** power for x in xs]
        plt.title('$f(x)=x^{}$'.format(power))
        plt.plot(xs, y)

    plt.show()


def power_function():
    xs = np.linspace(0, 3, 100)

    for idx in (0.5, 1, 2, 4):
        y = [x**idx for x in xs]
        plt.plot(xs, y, label="$y=x^{{{}}}$".format(idx))

    plt.ylim(0, 4)
    plt.legend(loc="upper left")
    plt.show()



if __name__ == '__main__':
    # polynomial_function()
    # even_power_function()
    # odd_power_function()
    power_function()

