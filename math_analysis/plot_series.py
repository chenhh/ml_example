# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v3
"""
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


def plot_lim_zero_div_series():
    N = 100
    xs = np.arange(1, N)
    ys = 1 / np.sqrt(xs)
    ys_sum = np.cumsum(ys)

    ax = plt.subplot(211)
    ax.plot(xs, ys)

    ax = plt.subplot(212)
    ax.plot(xs, ys_sum)

    plt.show()


def div_series():
    N = 100
    xs = np.arange(2, N)
    ys = np.sin(np.pi/xs)
    ys_sum = np.cumsum(ys)

    ax = plt.subplot(211)
    ax.plot(xs, ys)

    ax = plt.subplot(212)
    ax.plot(xs, ys_sum)

    plt.show()


def geometric_series():
    N = 20
    ks = np.arange(0, N)
    xs = (0.5, 1, 1.5)
    y0_s = np.cumsum(np.fromiter([xs[0]**k for k in ks], np.float))
    y1_s = np.cumsum(np.fromiter([xs[1] ** k for k in ks], np.float))
    y2_s = np.cumsum(np.fromiter([xs[2] ** k for k in ks], np.float))

    ax = plt.subplot(311)
    ax.plot(ks, y0_s)


    ax = plt.subplot(312)
    ax.plot(ks, y1_s)

    ax = plt.subplot(313)
    ax.plot(ks, y2_s)
    plt.show()


if __name__ == '__main__':
    # plot_lim_zero_div_series()
    # div_series()
    geometric_series()