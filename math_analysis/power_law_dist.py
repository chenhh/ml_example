# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
"""

import matplotlib.pyplot as plt
import numpy as np


def plot_normal_dist(mu, std, n_sample=5000):
    xs = np.random.normal(mu, std, n_sample)
    count, bins, ignored = plt.hist(xs, 100, normed=True)

    plt.title(r'Gaussian distribution $\mu$={}, $\sigma$={}'.format(mu, std))
    plt.plot(bins, 1 / (std * np.sqrt(2 * np.pi)) *
             np.exp(- (bins - mu) ** 2 / (2 * std ** 2)),
             linewidth=2, color='r')
    plt.show()


def plot_power_law_dist(alpha, n_sample=5000):
    xs = np.random.power(alpha, n_sample)

    count, bins, ignored = plt.hist(xs, 100, normed=True)

    plt.title(r'Power-law distribution $\alpha$={}'.format(alpha))
    plt.plot(bins, alpha * bins ** (alpha - 1),
             linewidth=2, color='r')
    plt.show()


def main():
    # plot_normal_dist(10, 4)
    plot_power_law_dist(2)


if __name__ == '__main__':
    main()
