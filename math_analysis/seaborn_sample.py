# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v3
"""

import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np


def linear_regression_chart():
    """
    https://seaborn.pydata.org/examples/anscombes_quartet.html
    """
    sns.set(style="ticks")

    # Load the example dataset for Anscombe's quartet
    # 4 classes of dataset
    df = sns.load_dataset("anscombe")
    print(df)

    # Show the results of a linear regression within each dataset
    sns.lmplot(x="x", y="y", col="dataset", hue="dataset", data=df,
               col_wrap=2, ci=None, palette="muted", size=4,
               scatter_kws={"s": 50, "alpha": 1})

    plt.show()


def bar_chart():
    """
    https://seaborn.pydata.org/examples/color_palettes.html
    """
    sns.set(style="white", context="talk")
    rs = np.random.RandomState(7)

    # Set up the matplotlib figure
    f, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(8, 6), sharex=True)

    # Generate some sequential data
    x = np.array(list("ABCDEFGHI"))
    y1 = np.arange(1, 10)
    sns.barplot(x, y1, palette="BuGn_d", ax=ax1)
    ax1.set_ylabel("Sequential")

    # Center the data to make it diverging
    y2 = y1 - 5
    sns.barplot(x, y2, palette="RdBu_r", ax=ax2)
    ax2.set_ylabel("Diverging")

    # Randomly reorder the data to make it qualitative
    y3 = rs.choice(y1, 9, replace=False)
    sns.barplot(x, y3, palette="Set3", ax=ax3)
    ax3.set_ylabel("Qualitative")

    # Finalize the plot
    sns.despine(bottom=True)
    plt.setp(f.axes, yticks=[])
    plt.tight_layout(h_pad=3)
    plt.show()


if __name__ == '__main__':
    # linear_regression_chart()
    bar_chart()
