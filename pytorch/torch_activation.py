# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v3
"""

import torch
import torch.nn.functional as F
from torch.autograd import Variable
import matplotlib.pyplot as plt

# define data
x = torch.linspace(-5, 5, 200)
x = Variable(x)
np_x = x.data.numpy()

# activation function
y_relu = F.relu(x).data.numpy()
y_sigmoid = F.sigmoid(x).data.numpy()
y_tanh = F.tanh(x).data.numpy()
y_softmax = F.softplus(x).data.numpy()

# plot activation function
plt.figure(1, figsize=(8, 6))
plt.subplot(221)
plt.plot(np_x, y_relu, c='red', label='relu')
plt.ylim((-1, 5))
plt.legend(loc='best')

plt.figure(1, figsize=(8, 6))
plt.subplot(222)
plt.plot(np_x, y_sigmoid, c='red', label='sigmoid')
plt.ylim((0, 1))
plt.legend(loc='best')

plt.figure(1, figsize=(8, 6))
plt.subplot(223)
plt.plot(np_x, y_tanh, c='red', label='tanh')
plt.ylim((-1, 1))
plt.legend(loc='best')

plt.figure(1, figsize=(8, 6))
plt.subplot(224)
plt.plot(np_x, y_softmax, c='red', label='softmax')
plt.ylim((-1, 5))
plt.legend(loc='best')

plt.show()
