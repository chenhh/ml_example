# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v3
"""

import torch
import matplotlib.pyplot as plt
import torch.nn.functional as F

from torch.autograd import Variable

# seed for random algorithm
torch.manual_seed(1)

# data
n_data = torch.ones(100, 2)

# data of first type
x0 = torch.normal(2 * n_data, 1)
# label of first type
y0 = torch.zeros(100)

# data of second type
x1 = torch.normal(-2 * n_data, 1)
# label of second type
y1 = torch.ones(100)

# x0, x1连接起来, 按维度0连接, 并指定数据的类型
x = torch.cat((x0, x1), 0).type(torch.FloatTensor)
# y0, y1连接, 由于只有一维, 因此没有指定维度, torch中标签类型必须为LongTensor
y = torch.cat((y0, y1), ).type(torch.LongTensor)

# x,y 转为变量, torch只支持变量的训练, 因为Variable中有grad
x, y = Variable(x), Variable(y)
# 绘制数据散点图
plt.scatter(x.data.numpy()[:, 0], x.data.numpy()[:, 1], c=y.data.numpy(),
            s=100, lw=0, cmap='RdYlGn')
plt.show()


# 定义分类网络
class Net(torch.nn.Module):

    def __init__(self, n_feature, n_hidden, n_output):
        super(Net, self).__init__()
        self.hidden = torch.nn.Linear(n_feature, n_hidden)
        self.prediction = torch.nn.Linear(n_hidden, n_output)

    def forward(self, x):
        x = F.relu(self.hidden(x))
        x = self.prediction(x)
        return x


# 定义网络
net = Net(n_feature=2, n_hidden=10, n_output=2)
print(net)

# Net (
#  (hidden): Linear (2 -> 10)
#  (prediction): Linear (10 -> 2)
# )

# 定义优化方法
optimizer = torch.optim.SGD(net.parameters(), lr=0.02)
# 定义损失函数
loss_func = torch.nn.CrossEntropyLoss()

plt.ion()

# 训练过程
for i in range(100):
    prediction = net(x)
    loss = loss_func(prediction, y)

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    if i % 2 == 0:
        plt.cla()
        # 获取概率最大的类别的索引
        prediction = torch.max(F.softmax(prediction, dim=1), 1)[1]
        # 将输出结果变为一维
        pred_y = prediction.data.numpy().squeeze()
        target_y = y.data.numpy()
        plt.scatter(x.data.numpy()[:, 0], x.data.numpy()[:, 1], c=pred_y,
                    s=100, lw=0, cmap='RdYlGn')
        # 计算准确率
        accuracy = sum(pred_y == target_y) / 200.0
        plt.text(1.5, -4, 'Accuracy=%.2f' % accuracy,
                 fontdict={'size': 10, 'color': 'red'})
        plt.pause(0.1)

plt.ioff()
plt.show()
