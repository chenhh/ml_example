# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v3
"""

import torch
import torch.utils.data.dataloader
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import (datasets, transforms)
from torch.autograd import Variable
import time

class pseudoInverse(object):
    def __init__(self,params,C=1e-2,forgettingfactor=1):
        self.params=list(params)
        self.is_cuda=self.params[len(self.params)-1].is_cuda
        self.C=C
        #self.params[len(self.params)-1].data.fill_(0)
        self.w=self.params[len(self.params)-1]
        self.w.data.fill_(0)#initialize output weight as zeros
        # For sequential learning in OS-ELM
        self.dimInput=self.params[len(self.params)-1].data.size()[1]
        self.forgettingfactor=forgettingfactor
        self.M=Variable(torch.inverse(self.C*torch.eye(self.dimInput)))

        if self.is_cuda:
            self.M=self.M.cuda()

    def initialize(self):
        self.M = Variable(torch.inverse(self.C * torch.eye(self.dimInput)))

        if self.is_cuda:
            self.M = self.M.cuda()
        self.w = self.params[len(self.params) - 1]
        self.w.data.fill_(0.0)


    def train(self,inputs,targets):
        oneHotTarget=self.oneHotVectorize(targets=targets)
        numSamples=inputs.size()[0]
        dimInput=inputs.size()[1]
        dimTarget=oneHotTarget.size()[1]


        xtx= torch.mm(inputs.t(),inputs)

        I = Variable(torch.eye(dimInput))
        if self.is_cuda:
            I=I.cuda()

        self.M = Variable(torch.inverse(xtx.data+self.C*I.data))
        w = torch.mm(self.M,inputs.t())
        w = torch.mm(w,oneHotTarget)

        #self.params[len(self.params)-1].data=w.t().data
        self.w.data=w.t().data

    def train_sequential(self,inputs,targets):
        oneHotTarget = self.oneHotVectorize(targets=targets)
        numSamples = inputs.size()[0]
        dimInput = inputs.size()[1]
        dimTarget = oneHotTarget.size()[1]


        I = Variable(torch.eye(numSamples))
        if self.is_cuda:
            I = I.cuda()

        self.M = (1/self.forgettingfactor) * self.M - torch.mm((1/self.forgettingfactor) * self.M,
                                             torch.mm(inputs.t(), torch.mm(Variable(torch.inverse(I.data + torch.mm(inputs, torch.mm((1/self.forgettingfactor)* self.M, inputs.t())).data)),
                                             torch.mm(inputs, (1/self.forgettingfactor)* self.M))))


        self.w.data += torch.mm(self.M,torch.mm(inputs.t(),oneHotTarget - torch.mm(inputs,self.w.t()))).t().data


    def oneHotVectorize(self,targets):
        oneHotTarget=torch.zeros(targets.size()[0],targets.max().data[0]+1)

        for i in xrange(targets.size()[0]):
            oneHotTarget[i][targets[i].data[0]]=1

        if self.is_cuda:
            oneHotTarget=oneHotTarget.cuda()
        oneHotTarget=Variable(oneHotTarget)

        return oneHotTarget


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(28 * 28, 7000)
        # self.bn = nn.BatchNorm1d(7000)

        # ELM do not use bias in the output layer.
        self.fc2 = nn.Linear(7000, 10,
                             bias=False)

    def forward(self, x):
        x = x.view(-1, self.num_flat_features(x))

        x = self.fc1(x)
        # x = self.bn(x)
        x = F.leaky_relu(x)
        x = self.fc2(x)
        return x

    def forwardToHidden(self, x):
        x = x.view(-1, self.num_flat_features(x))
        x = self.fc1(x)
        # x = self.bn(x)
        x = F.leaky_relu(x)
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features




def train(net,  optimizer, train_loader):
    init = time.time()
    net.train()
    correct = 0
    for batch_idx, (data, target) in enumerate(train_loader):
        if torch.cuda.is_available():
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data), Variable(target)

        hiddenOut = net.forwardToHidden(data)
        optimizer.train(inputs=hiddenOut, targets=target)
        output = model.forward(data)
        pred = output.data.max(1)[1]
        correct += pred.eq(target.data).cpu().sum()
    ending = time.time()
    print('training time: {:.2f}sec'.format(ending - init))
    print('\nTrain set accuracy: {}/{} ({:.0f}%)\n'.format(
        correct, len(train_loader.dataset),
        100. * correct / len(train_loader.dataset)))


def train_someBatch(batchidx=0):
    init = time.time()
    model.train()
    correct = 0
    for batch_idx, (data, target) in enumerate(train_loader):
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data), Variable(target)

        if batch_idx == batchidx:
            hiddenOut = model.forwardToHidden(data)
            optimizer.train(inputs=hiddenOut, targets=target)
        output = model.forward(data)
        pred = output.data.max(1)[1]
        correct += pred.eq(target.data).cpu().sum()

    ending = time.time()
    print('training time: {:.2f}sec'.format(ending - init))
    print('\nTrain set accuracy: {}/{} ({:.0f}%)\n'.format(
        correct, len(train_loader.dataset),
        100. * correct / len(train_loader.dataset)))


def test():
    model.eval()
    correct = 0
    for data, target in test_loader:
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data, volatile=True), Variable(target)
        output = model.forward(data)
        pred = output.data.max(1)[1]
        correct += pred.eq(target.data).cpu().sum()
    print('\nTest set accuracy: {}/{} ({:.0f}%)\n'.format(
        correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))


def train_sequential(starting_batch_idex=0):
    model.train()
    correct = 0
    for batch_idx, (data, target) in enumerate(train_loader):
        if batch_idx >= starting_batch_idex:
            if args.cuda:
                data, target = data.cuda(), target.cuda()
            data, target = Variable(data, volatile=True), Variable(target)
            hiddenOut = model.forwardToHidden(data)
            optimizer.train_sequential(hiddenOut, target)

            output = model.forward(data)
            pred = output.data.max(1)[1]
            correct += pred.eq(target.data).cpu().sum()
            print('\n{}st Batch train set accuracy: {}/{} ({:.0f}%)\n'.format(
                batch_idx,
                correct, (train_loader.batch_size * (batch_idx + 1)),
                100. * correct / (train_loader.batch_size * (batch_idx + 1))))

            test()


def ELM():
    net = Net()

    if torch.cuda.is_available():
        net.cuda()

    optimizer = pseudoInverse(params=net.parameters(), C=1e-2)
    train(net)


def OSELM():
    # Online Sequential ELM, batch_size is resized.
    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=True, download=True,
                       transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0,), (1,))
                       ])),
        batch_size=1000, shuffle=True, **kwargs)

    train_someBatch(batchidx=0)  # initialize phase; offline batch training
    train_sequential(
        starting_batch_idex=1)  # Sequential learning phase; online sequential training
    test()
