# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v3
"""

# Generative Adversarial Networks (GAN) example
# https://medium.com/@devnag/generative-adversarial-networks-gans-in-50-lines-of-code-pytorch-e81b79659e3f#.sch4xgsa9

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

# Data params
data_mean = 4
data_stddev = 1.25

# generator parameters
g_input_size = 1  # Random noise dimension coming into generator, per output vector
g_hidden_size = 50  # Generator complexity
g_output_size = 1  # size of generated output vector
g_learning_rate = 2e-4

# discriminator parameters
d_input_size = 200  # Mini-batch size - cardinality of distributions
d_hidden_size = 50  # Discriminator complexity
d_output_size = 1  # Single dimension for 'real' vs. 'fake'
d_learning_rate = 2e-4
mini_batch_size = d_input_size

# adam optimizer parameters
adam_betas = (0.9, 0.999)
num_epochs = 30000
print_interval = 200

# 'k' steps in the original GAN paper.
# Can put the discriminator on higher training freq than generator
d_steps = 1
g_steps = 1


# ##### DATA: Target data and generator input data

def get_distribution_sampler(mu, sigma, n):
    """
    real data distribution, shape: (1, n)
    """
    return torch.Tensor(np.random.normal(mu, sigma, (1, n)))


def get_generator_input_sampler(m, n):
    """
    生成網路G的隨機樣本z
    shape: (m.n)
    """
    # Uniform-dist data into generator, _NOT_ Gaussian
    # return lambda m, n: torch.rand(m, n)
    return torch.rand(m, n)


# ##### MODELS: Generator model and discriminator model

class Generator(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(Generator, self).__init__()
        self.weight1 = nn.Linear(input_size, hidden_size)
        self.weight2 = nn.Linear(hidden_size, hidden_size)
        self.weight3 = nn.Linear(hidden_size, output_size)

    def forward(self, x):
        x = F.elu(self.weight1(x))
        x = F.sigmoid(self.weight2(x))
        return self.weight3(x)


class Discriminator(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(Discriminator, self).__init__()
        self.weight1 = nn.Linear(input_size, hidden_size)
        self.weight2 = nn.Linear(hidden_size, hidden_size)
        self.weight3 = nn.Linear(hidden_size, output_size)

    def forward(self, x):
        x = F.elu(self.weight1(x))
        x = F.elu(self.weight2(x))
        return F.sigmoid(self.weight3(x))


def extract(v):
    """
    V, torch.Variable
    tensor to 1d list
    """
    return v.data.storage().tolist()


def stats(d):
    """
    """
    return [np.mean(d), np.std(d)]


def decorate_with_diffs(data, exponent=2.0):
    """
    data: torch.Tensor
    exponent: float
    """
    mean = torch.mean(data.data, 1)
    mean_broadcast = torch.mul(torch.ones(data.size()), mean.tolist()[0])
    diffs = torch.pow(data - Variable(mean_broadcast), exponent)
    return torch.cat([data, diffs], 1)


# main program

# 生成網路G與辨別網路D
G = Generator(input_size=g_input_size,
              hidden_size=g_hidden_size,
              output_size=g_output_size)
D = Discriminator(input_size=d_input_size * 2,
                  hidden_size=d_hidden_size,
                  output_size=d_output_size)
print(G)
print(D)

# Binary cross entropy: http://pytorch.org/docs/nn.html#bceloss
criterion = nn.BCELoss()
d_optimizer = optim.Adam(D.parameters(), lr=d_learning_rate, betas=adam_betas)
g_optimizer = optim.Adam(G.parameters(), lr=g_learning_rate, betas=adam_betas)

for epoch in range(num_epochs):

    for d_index in range(d_steps):
        # 1. Train D on real+fake
        D.zero_grad()

        #  1A: Train D on real
        # 由真實資料中取出 d_input_size個樣本, shape: (1, d_input_size)
        d_real_data = Variable(get_distribution_sampler(
            data_mean, data_stddev, d_input_size))

        # 把真實資料放入D中後, 求出相對應的答案
        d_real_decision = D(decorate_with_diffs(d_real_data))

        # 得出D分類錯誤的值, d_real_error: 正確答案為1, 但猜測為0
        d_real_error = criterion(d_real_decision,
                                 Variable(torch.ones(1)))  # ones = true

        # compute/store gradients, but don't change params
        d_real_error.backward()

        #  1B: Train D on fake
        # 得出G的原始隨機參數z
        d_gen_input = Variable(get_generator_input_sampler(
                        mini_batch_size, g_input_size))

        # 由G的參數z得出資料
        # detach to avoid training G on these labels
        d_fake_data = G(d_gen_input).detach()

        # 將生成資料代入D得出分類錯誤的值
        d_fake_decision = D(decorate_with_diffs(d_fake_data.t()))

        # 得出D分類錯誤的值, d_fake_error: 正確答案為0, 但猜測為1
        d_fake_error = criterion(d_fake_decision,
                                 Variable(torch.zeros(1)))  # zeros = fake
        d_fake_error.backward()

        # Only optimizes D's parameters;
        # changes based on stored gradients from backward()
        # 更新D的參數
        d_optimizer.step()

    for g_index in range(g_steps):
        # 2. Train G on D's response (but DO NOT train D on these labels)
        G.zero_grad()

        gen_input = Variable(
            get_generator_input_sampler(mini_batch_size, g_input_size))

        # 假資料, Variable, shape: (mini_batch_size, g_input_size)
        g_fake_data = G(gen_input)

        # D對G的假資料給出的答案, 正確為0, 錯誤為1
        dg_fake_decision = D(decorate_with_diffs(g_fake_data.t()))

        # 如果預測的結果為1時, 表示分類錯誤
        g_error = criterion(dg_fake_decision, Variable(
            torch.ones(1)))

        g_error.backward()
        g_optimizer.step()  # Only optimizes G's parameters

    if epoch % print_interval == 0:
        # d_real_error: 正確答案為1, 但猜測為0
        # d_fake_error: 正確答案為0, 但猜測為1
        disc = " D (real error {:.2%}/ fake error {:.2%})\n".format(
            extract(d_real_error)[0], extract(d_fake_error)[0], )

        real_mean, real_std = stats(extract(d_real_data))
        fake_mean, fake_std = stats(extract(d_fake_data))

        gen = "G [error] {:.4f} (Real: {:.4f}/{:.4f}, Fake: {:.4f}/{" \
              ":.4f})".format(
            extract(g_error)[0],
            real_mean, real_std, fake_mean, fake_std)

        print("iteration: {}".format(epoch) + disc + gen)
