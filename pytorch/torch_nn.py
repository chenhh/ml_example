# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>

"""

import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F


def cnn_example():
    """
    http://pytorch.org/tutorials/beginner/blitz/neural_networks_tutorial.html
    """

    class Net(nn.Module):

        def __init__(self):
            """
            define the architecture of the network.
            """
            super(Net, self).__init__()
            # 1 input image channel, 6 output channels, 5x5 square convolution
            # kernel
            self.conv1 = nn.Conv2d(1, 6, 5)
            self.conv2 = nn.Conv2d(6, 16, 5)
            # an affine operation: y = Wx + b
            self.fc1 = nn.Linear(16 * 5 * 5, 120)
            self.fc2 = nn.Linear(120, 84)
            self.fc3 = nn.Linear(84, 10)

        def forward(self, x):
            # Max pooling over a (2, 2) window
            x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
            # If the size is a square you can only specify a single number
            x = F.max_pool2d(F.relu(self.conv2(x)), 2)
            x = x.view(-1, self.num_flat_features(x))
            x = F.relu(self.fc1(x))
            x = F.relu(self.fc2(x))
            x = self.fc3(x)
            return x

        def num_flat_features(self, x):
            size = x.size()[1:]  # all dimensions except the batch dimension
            num_features = 1
            for s in size:
                num_features *= s
            return num_features

    net = Net()
    print(net)
    """
    Net(
        (conv1): Conv2d (1, 6, kernel_size=(5, 5), stride=(1, 1))
        (conv2): Conv2d (6, 16, kernel_size=(5, 5), stride=(1, 1))
        (fc1): Linear(in_features=400, out_features=120)
        (fc2): Linear(in_features=120, out_features=84)
        (fc3): Linear(in_features=84, out_features=10)
    )
    """
    ########################################################################
    # You just have to define the ``forward`` function, and the ``backward``
    # function (where gradients are computed) is automatically defined for you
    # using ``autograd``.
    # You can use any of the Tensor operations in the ``forward`` function.
    #
    # The learnable parameters of a model are returned by ``net.parameters()``

    params = list(net.parameters())
    print(len(params))
    print(params[0].size())  # conv1's .weight

    ########################################################################
    # The input to the forward is an ``autograd.Variable``, and so is the output.
    # Note: Expected input size to this net(LeNet) is 32x32. To use this net on
    # MNIST dataset,please resize the images from the dataset to 32x32.

    input = Variable(torch.randn(1, 1, 32, 32))
    out = net(input)
    print(out)

    ########################################################################
    # Zero the gradient buffers of all parameters and backprops with random
    # gradients:
    net.zero_grad()
    out.backward(torch.randn(1, 10))

    ########################################################################
    # .. note::
    #
    #     ``torch.nn`` only supports mini-batches The entire ``torch.nn``
    #     package only supports inputs that are a mini-batch of samples, and not
    #     a single sample.
    #
    #     For example, ``nn.Conv2d`` will take in a 4D Tensor of
    #     ``nSamples x nChannels x Height x Width``.
    #
    #     If you have a single sample, just use ``input.unsqueeze(0)`` to add
    #     a fake batch dimension.
    #
    # Before proceeding further, let's recap all the classes you’ve seen so far.
    #
    # **Recap:**
    #   -  ``torch.Tensor`` - A *multi-dimensional array*.
    #   -  ``autograd.Variable`` - *Wraps a Tensor and records the history of
    #      operations* applied to it. Has the same API as a ``Tensor``, with
    #      some additions like ``backward()``. Also *holds the gradient*
    #      w.r.t. the tensor.
    #   -  ``nn.Module`` - Neural network module. *Convenient way of
    #      encapsulating parameters*, with helpers for moving them to GPU,
    #      exporting, loading, etc.
    #   -  ``nn.Parameter`` - A kind of Variable, that is *automatically
    #      registered as a parameter when assigned as an attribute to a*
    #      ``Module``.
    #   -  ``autograd.Function`` - Implements *forward and backward definitions
    #      of an autograd operation*. Every ``Variable`` operation, creates at
    #      least a single ``Function`` node, that connects to functions that
    #      created a ``Variable`` and *encodes its history*.
    #
    # **At this point, we covered:**
    #   -  Defining a neural network
    #   -  Processing inputs and calling backward.
    #
    # **Still Left:**
    #   -  Computing the loss
    #   -  Updating the weights of the network
    #
    # Loss Function
    # -------------
    # A loss function takes the (output, target) pair of inputs, and computes a
    # value that estimates how far away the output is from the target.
    #
    # There are several different
    # `loss functions <http://pytorch.org/docs/nn.html#loss-functions>`_ under the
    # nn package .
    # A simple loss is: ``nn.MSELoss`` which computes the mean-squared error
    # between the input and the target.
    #
    # For example:

    output = net(input)
    target = Variable(torch.arange(1, 11))  # a dummy target, for example
    criterion = nn.MSELoss()

    loss = criterion(output, target)
    print(loss)

    ########################################################################
    # Now, if you follow ``loss`` in the backward direction, using it’s
    # ``.grad_fn`` attribute, you will see a graph of computations that looks
    # like this:
    #
    # ::
    #
    #     input -> conv2d -> relu -> maxpool2d -> conv2d -> relu -> maxpool2d
    #           -> view -> linear -> relu -> linear -> relu -> linear
    #           -> MSELoss
    #           -> loss
    #
    # So, when we call ``loss.backward()``, the whole graph is differentiated
    # w.r.t. the loss, and all Variables in the graph will have their
    # ``.grad`` Variable accumulated with the gradient.
    #
    # For illustration, let us follow a few steps backward:

    print(loss.grad_fn)  # MSELoss
    print(loss.grad_fn.next_functions[0][0])  # Linear
    print(loss.grad_fn.next_functions[0][0].next_functions[0][0])  # ReLU

    ########################################################################
    # Backprop
    # --------
    # To backpropagate the error all we have to do is to ``loss.backward()``.
    # You need to clear the existing gradients though, else gradients will be
    # accumulated to existing gradients
    #
    #
    # Now we shall call ``loss.backward()``, and have a look at conv1's bias
    # gradients before and after the backward.

    net.zero_grad()  # zeroes the gradient buffers of all parameters

    print('conv1.bias.grad before backward')
    print(net.conv1.bias.grad)

    loss.backward()

    print('conv1.bias.grad after backward')
    print(net.conv1.bias.grad)

    ########################################################################
    # Now, we have seen how to use loss functions.
    #
    # **Read Later:**
    #
    #   The neural network package contains various modules and loss functions
    #   that form the building blocks of deep neural networks. A full list with
    #   documentation is `here <http://pytorch.org/docs/nn>`_
    #
    # **The only thing left to learn is:**
    #
    #   - updating the weights of the network
    #
    # Update the weights
    # ------------------
    # The simplest update rule used in practice is the Stochastic Gradient
    # Descent (SGD):
    #
    #      ``weight = weight - learning_rate * gradient``
    #
    # We can implement this using simple python code:
    #
    # .. code:: python
    #
    #     learning_rate = 0.01
    #     for f in net.parameters():
    #         f.data.sub_(f.grad.data * learning_rate)
    #
    # However, as you use neural networks, you want to use various different
    # update rules such as SGD, Nesterov-SGD, Adam, RMSProp, etc.
    # To enable this, we built a small package: ``torch.optim`` that
    # implements all these methods. Using it is very simple:

    import torch.optim as optim

    # create your optimizer
    optimizer = optim.SGD(net.parameters(), lr=0.01)

    # in your training loop:
    optimizer.zero_grad()  # zero the gradient buffers
    output = net(input)
    loss = criterion(output, target)
    loss.backward()
    optimizer.step()  # Does the update


def xor_example():
    class Net(nn.Module):
        def __init__(self):
            super(Net, self).__init__()
            self.fc1 = nn.Linear(2, 50)  # 2 Input noses, 50 in middle layers
            self.fc2 = nn.Linear(50, 1)  # 50 middle layer, 1 output nodes
            self.rl1 = nn.ReLU()
            self.rl2 = nn.ReLU()

        def forward(self, x):
            x = self.fc1(x)
            x = self.rl1(x)
            x = self.fc2(x)
            x = self.rl2(x)
            return x

    ## Create Network

    net = Net()
    # print net

    ## Optimization and Loss

    # criterion = nn.CrossEntropyLoss() # use a Classification Cross-Entropy loss
    criterion = nn.MSELoss()
    # criterion = nn.L1Loss()
    # criterion = nn.NLLLoss()
    # criterion = nn.BCELoss()
    optimizer = torch.optim.SGD(net.parameters(), lr=0.001, momentum=0.1)
    # optimizer = optim.Adam(net.parameters(), lr=0.01)

    trainingdataX = [
        [[0.01, 0.01], [0.01, 0.90], [0.90, 0.01], [0.95, 0.95]],
        [[0.02, 0.03], [0.04, 0.95], [0.97, 0.02], [0.96, 0.95]]]
    trainingdataY = [[[0.01], [0.90], [0.90], [0.01]],
                     [[0.04], [0.97], [0.98], [0.1]]]
    NumEpoches = 2000
    for epoch in range(NumEpoches):

        running_loss = 0.0
        for i, data in enumerate(trainingdataX, 0):
            inputs = data
            labels = trainingdataY[i]
            inputs = Variable(torch.FloatTensor(inputs))
            labels = Variable(torch.FloatTensor(labels))
            optimizer.zero_grad()
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            running_loss += loss.data[0]
            if i % 1000 == 0:
                print
                "loss: ", running_loss
                running_loss = 0.0
    print ("Finished training...")
    print (net(Variable(torch.FloatTensor(trainingdataX[0]))))


if __name__ == '__main__':
    # cnn_example()
    xor_example()
