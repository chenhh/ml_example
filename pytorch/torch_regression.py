# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v3
"""

import torch
import torch.nn.functional as F
import matplotlib.pyplot as plt
from torch.autograd import Variable

# generating data, the NN requires the data is 2-dimension.
# torch.linspace(-1, 1, 100), shape: 100
# x,  shape: (100,1)
x = torch.unsqueeze(torch.linspace(-1, 1, 100), dim=1)

# noise y data, shape: (100, 1)
y = x.pow(2) + 0.2 * torch.rand(x.size())

# change to Variable
x, y = Variable(x), Variable(y)

# plt.scatter(x.data.numpy(), y.data.numpy())
# plt.show()


# define NN
class Net(torch.nn.Module):

    def __init__(self, n_features, n_hidden, n_output):
        super(Net, self).__init__()
        # weight matrices, default values are Gaussian random values.
        # weight matrix from input to hidden
        self.hidden = torch.nn.Linear(n_features, n_hidden)

        # weight matrix from hidden to outout
        self.predict = torch.nn.Linear(n_hidden, n_output)

    def forward(self, x):
        # x is the input data.

        # the x is output vector after hidden layer.
        x = F.relu(self.hidden(x))

        # the y is output vector after output layer.
        y = self.predict(x)
        return y


# get the NN object.
net = Net(1, 10, 1)
print(net)

# Net (
#  (hidden): Linear (1 -> 10)
#  (predict): Linear (10 -> 1)
# )

# choose the optimization function
# net.parameters() are the parameters in the NN
optimizer = torch.optim.SGD(net.parameters(), lr=0.5)

# choose loss function
loss_func = torch.nn.MSELoss()

# interactive mode on
plt.ion()

# start training
for i in range(500):
    # input data to the NN and get the prediction value
    prediction = net(x)

    # compute the loss between prediction and real value
    loss = loss_func(prediction, y)

    # clear the gradient
    optimizer.zero_grad()
    # back-propagation
    loss.backward()
    # update gradient
    optimizer.step()

    if i % 5 == 0:
        plt.cla()
        plt.scatter(x.data.numpy(), y.data.numpy())
        plt.plot(x.data.numpy(), prediction.data.numpy(), 'r-', lw=5)
        plt.text(0.5, 0, 'Loss=%.4f' % loss.data[0],
                 fontdict={'size': 10, 'color': 'red'})
        plt.pause(0.1)

# interactive mode off
plt.ioff()
plt.show()
