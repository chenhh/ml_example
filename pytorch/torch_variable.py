# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v3
"""

import torch
from torch.autograd import Variable


# basic example
def basic_example1():
    print("*" * 10 + "basic example" + "*" * 10)

    # it requires to set requires_grad=True, default is False
    x = Variable(torch.ones(2, 2), requires_grad=True)
    print("variable x:\n", x)
    y = x + 2
    print("variable y:\n", y)  # variable
    print(y.grad)  # None

    z = y * y * 3
    out = z.mean()
    # print(z, out)

    out.backward()
    # d(out)/dx = d(out)/dz * dz/dy * dy/dx = 1/4 * 6y * 1 = 6y/4
    print("x.grad:\n", x.grad)


def basic_example2():
    print("*" * 10 + "basic example 2" + "*" * 10)
    tensor = torch.FloatTensor([[1, 2], [3, 4]])

    # it requires to set requires_grad=True, default is False
    variable = Variable(tensor, requires_grad=True)

    print("Tensor and variable:\n")
    print(tensor)
    print(variable)

    print("* is the element-wise product:\n")
    print(tensor * tensor)
    print(variable * variable)

    # 计算x^2的均值
    tensor_mean = torch.mean(tensor * tensor)
    variable_mean = torch.mean(variable * variable)
    print(tensor_mean)
    print(variable_mean)

    # variable进行反向传播
    # 梯度计算如下：
    # variable_mean = 1/4 * sum(variable * variable)
    # d(variable_mean)/d(variable) = 1/4 * 2 * variable = 1/2 * variable
    variable_mean.backward()

    # 输出variable中的梯度
    print("gradient vector:")
    print(variable.grad)

    # 输出variable中的data, data是tensor
    print(variable.data)

    # 需要求导的话，requires_grad=True属性是必须的。
    w1 = Variable(torch.Tensor([1.0, 2.0, 3.0]), requires_grad=True)
    w2 = Variable(torch.Tensor([1.0, 2.0, 3.0]), requires_grad=True)

    print(w1.grad)  # 0.3 版本打印的是 None
    print(w2.grad)  # 0.3 版本打印的是 None

    # 使用d.backward()求Variable的梯度的时候，Variable.grad是累加的即:
    # Variable.grad=Variable.grad+new_grad
    d = torch.mean(w1)
    d.backward(retain_graph=True)
    print(w1.grad)

    # cumulative gradient
    d.backward(retain_graph=True)
    print(w1.grad)

    # reset gradient
    w1.grad.data.zero_()
    d.backward()
    print(w1.grad)


if __name__ == '__main__':
    basic_example1()
    # basic_example2()
