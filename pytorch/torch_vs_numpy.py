# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v3
"""

import torch
import numpy as np


def new_dimension():
    # shape: (10,)
    data = np.arange(10)

    # 2d-array
    data_2d = data[:, np.newaxis]
    # shape: (10, 1)
    print(data_2d.shape)

    # 2d-tensor
    tensor_2d = torch.unsqueeze(torch.Tensor(data), dim=1)
    # torch.Size([10, 1])
    print(tensor_2d.size())


def matrix_mul():
    a = np.arange(10).reshape((5, 2))
    b = np.ones(2)[:, np.newaxis]
    print(a.dot(b))
    print(np.matmul(a, b))

    u = torch.Tensor(a)
    v = torch.Tensor(b)
    print(torch.mm(u, v))
    # print(u.dot(v))


def reshape():
    d = np.arange(10).reshape((-1, 5))
    print(d)

    t = torch.arange(10).view(-1, 5)
    print(t)


if __name__ == '__main__':
    # new_dimension()
    # matrix_mul()
    reshape()
