# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
Bayesian optimization example
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sp
from sklearn import gaussian_process

def func(x):
    ''' unknown function
    '''
    mu1 = 0.3
    var1 = 0.01
    mu2 = 0.6
    var2 = 0.03
    y = (1/np.sqrt(2*np.pi*var1)) * np.exp(-(x-mu1)**2./var1) + (1/np.sqrt(
        2*np.pi*var2)) * np.exp(-(x-mu2)**2./var2)
    return y

def main():
    # unknown function
    x = np.linspace(0, 1, 200)
    y = func(x)
    plt.plot(x, y, '-r')

    # observations
    xtr = np.array([0.1, 0.5, 0.8])
    ytr = func(xtr)

    plt.title('unknown relationship')
    plt.plot(xtr, ytr, 'ko', markersize=10)

    D = np.atleast_2d(xtr).T
    y_D = func(xtr)
    plt.show()

    gp = gaussian_process.GaussianProcess(theta0=0.05, thetaL=0.01, thetaU=1)
    gp.fit(D, y_D)


    xt = np.atleast_2d(x).T
    y_pred, sigma2_pred = gp.predict(xt, eval_MSE=True)

    plt.plot(x, y, '-r')
    plt.plot(xt, y_pred, '-b')
    plt.plot(xt, y_pred + 2 * np.sqrt(sigma2_pred), '-g')
    plt.plot(xtr, ytr, 'ko', markersize=10)

    xtr2 = np.random.sample(40)
    D = np.atleast_2d(xtr2).T
    y_D = func(xtr2)
    plt.show()

    gp = gaussian_process.GaussianProcess(theta0=0.05, thetaL=0.01, thetaU=1)
    gp.fit(D, y_D)

    xt = np.atleast_2d(x).T
    y_pred, sigma2_pred = gp.predict(xt, eval_MSE=True)

    plt.plot(x, y, '-r')
    plt.plot(xt, y_pred, '-b')
    plt.plot(xt, y_pred + 2 * np.sqrt(sigma2_pred), '-g')
    plt.show()

if __name__ == '__main__':
    main()