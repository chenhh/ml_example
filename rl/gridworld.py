# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""

import numpy as np
import numpy.linalg as la
from time import time

def gridworld(gamma=1):
    # goal: move from state[0, 0] to state[3,3]
    # random policy, each state has 4 possible actions with equiprobable.
    # index z: 0: up, 1: right, 2: down, 3:left

    values = np.zeros(16)
    rewards = np.ones(16) * -1.
    rewards[0] = 0
    rewards[15] = 0

    probs = np.array([
        # probs[0]
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        # probs[1]
        [0.25, 0.25, 0.25, 0, 0, 0.25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        # probs[2]
        [0, 0.25, 0.25, 0.25, 0, 0, 0.25, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        # probs[3]
        [0, 0, 0.25, 0.5, 0, 0, 0, 0.25, 0, 0, 0, 0, 0, 0, 0, 0],
        # probs[4]
        [0.25, 0, 0, 0, 0.25, 0.25, 0, 0, 0.25, 0, 0, 0, 0, 0, 0, 0],
        # probs[5]
        [0, 0.25, 0, 0, 0.25, 0, 0.25, 0, 0, 0.25, 0, 0, 0, 0, 0, 0],
        # probs[6]
        [0, 0, 0.25, 0, 0, 0.25, 0, 0.25, 0, 0, 0.25, 0, 0, 0, 0, 0],
        # probs[7]
        [0, 0, 0, 0.25, 0, 0, 0.25, 0.25, 0, 0, 0, 0.25, 0, 0, 0, 0],
        # probs[8]
        [0, 0, 0, 0, 0.25, 0, 0, 0, 0.25, 0.25, 0, 0, 0.25, 0, 0, 0],
        # probs[9]
        [0, 0, 0, 0, 0, 0.25, 0, 0, 0.25, 0, 0.25, 0, 0, 0.25, 0, 0],
        # probs[10]
        [0, 0, 0, 0, 0, 0, 0.25, 0, 0, 0.25, 0, 0.25, 0, 0, 0.25, 0],
        # probs[11]
        [0, 0, 0, 0, 0, 0, 0, 0.25, 0, 0, 0.25, 0.25, 0, 0, 0, 0.25],
        # probs[12]
        [0, 0, 0, 0, 0, 0, 0, 0, 0.25, 0, 0, 0, 0.5, 0.25, 0, 0],
        # probs[13]
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0.25, 0, 0, 0.25, 0.25, 0.25, 0],
        # probs[14]
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.25, 0, 0, 0.25, 0.25, 0.25],
        # probs[15]
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ])
    # pprint(probs)
    A = np.eye(16) - gamma * probs
    b = rewards
    t1 = time()
    print(np.dot(la.inv(A), b))
    t2 = time()
    print("inverse matrix : {:.4f} secs".format(t2-t1))

    t3 = time()
    print (la.solve(A,b))
    t4 = time()
    print("linear equation : {:.4f} secs".format(t4 - t3))

if __name__ == '__main__':
    gridworld()
