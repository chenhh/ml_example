# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2

David Silver's reinforcement learning course 2.
student Markov decision process
"""

import numpy as np
import numpy.linalg as la


def mdp(gamma=1):
    r = np.array([-0.5, -1.5, -1, 5.5, 0])
    p = np.array([[0.5, 0.5, 0., 0., 0.],
                  [0.5, 0., 0.5, 0., 0.],
                  [0., 0., 0., 0.5, 0.5],
                  [0., 0.2 * 0.5, 0.4 * 0.5, 0.4 * 0.5, 0.5],
                  [0., 0., 0., 0., 0.]])
    print(np.dot(la.inv(np.eye(5) - gamma * p), r))


if __name__ == '__main__':
    mdp(0)