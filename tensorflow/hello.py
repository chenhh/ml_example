# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2

http://wiki.jikexueyuan.com/project/tensorflow-zh/get_started/os_setup.html
"""
import tensorflow as tf

# string
hello = tf.constant('Hello, TensorFlow!')
with tf.Session() as sess:
    print(sess.run(hello))

# addition
a = tf.constant(10, dtype=tf.float64)
b = tf.constant(32, dtype=tf.float64)
c = a + b
with tf.Session() as sess:
    print(sess.run(c))  # 42.0

# matrix multiplication
matrix1 = tf.constant([[3., 3.]])
matrix2 = tf.constant([[2.], [2.]])
prod = tf.matmul(matrix1, matrix2)
with tf.Session() as sess:
    print(sess.run(prod))  # [[12.]]

# input
input1 = tf.placeholder(tf.float64)
input2 = tf.placeholder(tf.float64)
# output = tf.mul(input1, input2)
output = input1 * input2

with tf.Session() as sess:
    # [array([ 14.])]
    with tf.device("/cpu:0"):
        print(sess.run([output], feed_dict={input1: [7.], input2: [2.]}))

x = tf.constant([35, 40, 45], name='x')
y = tf.Variable(x + 5, name='y')
model = tf.global_variables_initializer()
with tf.Session() as session:
    session.run(model)
    print(session.run(y))

import numpy as np
data = np.random.randint(1000, size=10000)

x = tf.constant(data, name='x')
y = tf.Variable(5*x*x - 3*x + 15, name='y')
model = tf.global_variables_initializer()
with tf.Session() as session:
    session.run(model)
    print(session.run(y))