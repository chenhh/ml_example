# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""
import tensorflow as tf
import numpy as np

N_DATA = 100
N_EPOCH = 1000

# create random input data
x_data = np.random.rand(N_DATA).astype(np.float32)

# output data, weight:0.3  biases:0.3
y_data = x_data * 0.1 + 0.3

# tensorflow default graph
# weight: a uniform random variable in [-1, 1]
Weights = tf.Variable(tf.random_uniform([1], -1.0, 1.0))

# bias:　zero
biases = tf.Variable(tf.zeros([1]))

# learning function
y = Weights * x_data + biases

# loss function, average loss between true and predict values
loss = tf.reduce_mean(tf.square(y - y_data))

# optimizating algorithm
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.5)
# optimizer = tf.train.ProximalGradientDescentOptimizer(learning_rate=0.5)
# optimizer = tf.train.AdamOptimizer(learning_rate=0.05)
# optimizer = tf.train.FtrlOptimizer(learning_rate=0.5)


train = optimizer.minimize(loss)
init = tf.global_variables_initializer()

with tf.Session() as sess:
    # initialize all variables
    sess.run(init)
    for step in range(1, N_EPOCH + 1):
        sess.run(train)
        if step % 50 == 0:
            print(step, sess.run(Weights), sess.run(biases))

