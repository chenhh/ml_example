# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v3
"""

import tensorflow as tf

input1 = tf.placeholder(tf.float32)
input2 = tf.placeholder(tf.float32)

mul = input1 * input2

with tf.Session() as sess:
    # feed_dcit 傳值進入placeholder
    print(sess.run(mul, feed_dict={input1: [3, ], input2: [4, ]}))
