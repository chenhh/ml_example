# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v3
"""

import tensorflow as tf

mat1 = tf.constant([[3,3]])
mat2 = tf.constant([[2],
                    [2]])

# matrix multiplication
# the same as np.dot(mat1, mat2)
prod = tf.matmul(mat1, mat2)

with tf.Session() as sess:
    print(sess.run(prod))