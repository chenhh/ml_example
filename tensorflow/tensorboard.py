# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2

use the command: tensorboard --logdir=path/to/log-directory
"""

import tensorflow as tf

x = tf.constant(35, name='x')
print(x)
y = tf.Variable(x + 5, name='y')

with tf.Session() as session:
    merged = tf.summary.merge_all()
    writer = tf.summary.FileWriter(r"e:", session.graph)
    model = tf.global_variables_initializer()
    session.run(model)
    print(session.run(y))
