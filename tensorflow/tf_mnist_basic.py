# -*- coding: utf-8 -*-
"""
Author: Hung-Hsin Chen <chenhh@par.cse.nsysu.edu.tw>
License: GPL v2
"""
from time import time
from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf

# download mnist
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)
x
x = tf.placeholder(tf.float32, [None, 784])
W = tf.Variable(tf.zeros([784, 10]))
b = tf.Variable(tf.zeros([10]))
y = tf.nn.softmax(tf.matmul(x, W) + b)

real_y = tf.placeholder("float", [None, 10])
cross_entropy = -tf.reduce_sum(real_y * tf.log(y))
train_step = tf.train.GradientDescentOptimizer(0.01).minimize(cross_entropy)
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    for tdx in range(1000):
        t_s = time()
        batch_xs, batch_ys = mnist.train.next_batch(100)
        sess.run(train_step, feed_dict={x: batch_xs, real_y: batch_ys})
        correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(real_y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
        print(sess.run(accuracy, feed_dict={x: mnist.test.images,
                                            real_y: mnist.test.labels}))
        print("batch:{}, time:{:.4f}".format(tdx, time() - t_s))
